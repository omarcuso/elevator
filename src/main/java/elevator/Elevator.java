package elevator;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Elevator {

	int floorStay;
	boolean isDown;

	public int getFloorStay() {
		return floorStay;
	}

	public void setFloorStay(int floorStay) {
		this.floorStay = floorStay;
	}

	public boolean getIsDown() {
		return isDown;
	}

	public void setIsDown(boolean isDown) {
		this.isDown = isDown;
	}

	public static void main(String[] args) {

		// TODO Auto-generated method stub
		// Input ผู้โดยสาร
		List<Passenger> passengerList = new ArrayList<Passenger>();
		Passenger p1 = new Passenger();
		p1.setStay(1);
		p1.setTarget(5);
		passengerList.add(p1);
		Passenger p2 = new Passenger();
		p2.setStay(2);
		p2.setTarget(6);
		passengerList.add(p2);
		Passenger p3 = new Passenger();
		p3.setStay(5);
		p3.setTarget(7);
		passengerList.add(p3);
		Passenger p4 = new Passenger();
		p4.setStay(7);
		p4.setTarget(8);
		passengerList.add(p4);
		Passenger p5 = new Passenger();
		p5.setStay(6);
		p5.setTarget(4);
		passengerList.add(p5);
		Passenger p6 = new Passenger();
		p6.setStay(6);
		p6.setTarget(3);
		passengerList.add(p6);

		Set<Integer> dFloor = new TreeSet<Integer>().descendingSet();
		Set<Integer> uFloor = new TreeSet<Integer>(); // treeset
														// เป็นเซทที่ตัดตัวซ้ำและเรียงลำดับใหม่
		Set<Integer> allStayFloor = new TreeSet<Integer>();
		List<Integer> allStayFloorLs = new ArrayList<Integer>();

		for (Passenger p : passengerList) {
			if (p.isDown()) {

				dFloor.add(p.getStay());
				dFloor.add(p.getTarget());
			} else {

				uFloor.add(p.getStay());
				uFloor.add(p.getTarget());
			}
			allStayFloor.add(p.getStay());
		}

		for (int s : allStayFloor) {
			allStayFloorLs.add(s);
		}

		Elevator e1 = new Elevator();
		Elevator e2 = new Elevator();

		int higthFloor = allStayFloorLs.get(allStayFloorLs.size() - 1);
		// int lowFloor = allStayFloorLs.get(0);

		e1.setFloorStay(4);
		e2.setFloorStay(7);

		if ((Math.abs(higthFloor - e1.getFloorStay())) < Math.abs(higthFloor - e2.getFloorStay())) {
			e2.setIsDown(false);
			e1.setIsDown(true);
		} else {
			e2.setIsDown(true);
			e1.setIsDown(false);
		}

		if (e1.getIsDown()) {
			System.out.println("Elevator 2nd stand by floor :" + e2.getFloorStay());
			for (int x : uFloor) {
				System.out.println("Elevator 2nd go to Floor " + x);
			}
			System.out.println("Elevator 1st stand by floor :" + e1.getFloorStay());
			for (int x : dFloor) {
				System.out.println("Elevator 1st go to Floor " + x);
			}
		} else {
			System.out.println("Elevator 1st stand by floor :" + e1.getFloorStay());
			for (int x : uFloor) {
				System.out.println("Elevator 1st go to Floor " + x);
			}
			System.out.println("Elevator 2nd stand by floor :" + e2.getFloorStay());
			for (int x : dFloor) {
				System.out.println("Elevator 2nd go to Floor " + x);
			}
		}

	}

}
