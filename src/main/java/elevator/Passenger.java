package elevator;

public class Passenger {

	private	int stay;
	private	int target;
	

	public boolean isDown(){
		if(stay<target)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public int getStay() {
		return stay;
	}
	
	public void setStay(int stay) {
		this.stay = stay;
	}
	
	public int getTarget() {
		return target;
	}
	
	public void setTarget(int target) {
		this.target = target;
	}
	
	
}
